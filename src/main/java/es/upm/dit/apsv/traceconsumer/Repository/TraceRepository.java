package es.upm.dit.apsv.traceconsumer.Repository;

import org.springframework.data.repository.CrudRepository;
import es.upm.dit.apsv.traceconsumer.model.Trace;

public interface TraceRepository extends CrudRepository<Trace,String> {
}
